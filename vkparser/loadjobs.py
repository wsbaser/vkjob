"""
@author: Kirill Python
@contact: http://vk.com/python273
@license Apache License, Version 2.0, see LICENSE file

Copyright (C) 2014
"""

import vk_api
from pyquery import PyQuery as pq

def main():
    """ Пример получения последнего сообщения со стены """

    login, password = 'wsbaser@gmail.com', 'GabriellaCook88'

    try:
        vk = vk_api.VkApi(login, password)  # Авторизируемся
    except vk_api.AuthorizationError as error_msg:
        print(error_msg)  # В случае ошибки выведем сообщение
        return  # и выйдем

    # get employees
    response = vk.get('http://vk.com/search?c[company]=jetbrains&c[name]=0&c[section]=people')
    with open("employees.html","wt") as f:
        f.write(response.text)
    q=pq(response.text);
    employees = q("#results .name>a")

    # get career list
    for employee in employees:
        print("getting career for employee: "+employee.attrib['href']+"...")
        url="http://vk.com"+employee.attrib['href']
        response = vk.get(url)
        q = pq(response.text)
        e = q('.profile_career_row div:nth-of-type(2)')
        cityandtime = q('.profile_career_row div:nth-of-type(2)')[0].text
        company = q('.profile_career_row div:nth-of-type(2) a:nth-of-type(1)')[0].text
        position = q('.profile_career_row div:nth-of-type(2) a:nth-of-type(2)')[0].text
        print(cityandtime+", "+company+", "+position)

if __name__ == '__main__':
    main()
